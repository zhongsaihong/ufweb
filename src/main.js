cats = require('./cat.js');
console.log(cats);
//import vue and vue-rounter
const Vue = require('vue/dist/vue.js');
const VueRouter = require('vue-router');
Vue.use(VueRouter);

//import components
//vue debug mode
Vue.config.debug = true;
//var App = Vue.extend({});//root components


const userLang = navigator.language || navigator.userLanguage,
	  setLang = userLang.substring(0, 2)=="zh" ? "cn": "en";

const routes = [
	{
		path: '/:lang',
		component: require('./components/common.vue'),
		children: [
			{
				path:'',
				name:'app',
				component:require('./components/app.vue')
			},{
				path:'about/:position',
				name:'about',
				component:require('./components/about.vue')
			},{
				path:'support',
				component:require('./components/supportNav.vue'),
				children:[
					{
						path:'',
						name:'download',
						component:require('./components/support.vue')
					},{
						path:'help',
						name:'help',
						component:require('./components/help.vue')
					},{
						path:'question',
						name:'question',
						component:require('./components/question.vue')
					}
				]
			},{
				path:'store',
				name:'store',
				component:require('./components/store.vue')
			},{
				path:'community',
				name:'community',
				component:require('./components/store.vue')
			},{
				path:'product/:id',
				name:'product',
				component:require('./components/product.vue')
			},{
				path:'uarm1',
				name:'uarm1',
				component:require('./components/uarm1.vue')
			},{
				path:'uarmswift',
				name:'uarmswift',
				component:require('./components/uarmswift.vue')
			},{
				path:'cart',
				name:'cart',
				component:require('./components/cart.vue')
			},{
				path:'login',
				name:'login',
				component:require('./components/login.vue')
			}
		]
	},
	{
		path:'*',
		redirect:{name:'app', params:{lang:setLang}}
	}
];
const scrollBehavior = (to, from, savedPosition) => {
	const position = {}
	if (to.hash) {
	  position.selector = to.hash
	  }else{
		  position.x = 0
	      position.y = 0
	  }
	  return position

}
const router = new VueRouter({
	//mode: 'history',
	routes,
	//scrollBehavior
  // istory: false,
  // saveScrollPosition: false
});

// router.beforeEach((transition) => {
// 	window.scrollTo(0, 0)
//     transition.next()
// })

window.onload = function () {
	const app = new Vue({
		router
	}).$mount('#app');
}
