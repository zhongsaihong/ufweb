 //var vue = require('vue-loader');
 var webpack = require("webpack");
 var OpenBrowserPlugin=require('open-browser-webpack-plugin');
 module.exports = {
    entry: './src/main.js',
    output: {
        path: './bin',
        publicPath: 'http://mycdn.com/', // This is used to generate URLs to e.g. images
        filename: 'app.bundle.js'
    },
	module: {
		  preLoaders: [
	        { test: /\.json$/, exclude: /node_modules/, loader: 'json'},
	    ],
	    loaders: [
	   		{ test: /\.vue$/, loader: 'vue'},
			{ test: /\.js$/, loader: 'babel', exclude: /node_modules/},
	    	{ test: /\.less$/, loader: 'style-loader!css-loader!less-loader' }, // use ! to chain loaders
	    	{ test: /\.css$/, loader: 'css-loader' },
	      	{ test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192' }, // inline base64 URLs for <=8k images, direct URLs for the rest
	      	{ test: /\.(woff|woff2)$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
	      	{ test: /\.ttf$/, loader: "file-loader" },
	      	{ test: /\.eot$/, loader: "file-loader" },
	      	{ test: /\.svg$/, loader: "file-loader" },
	   ]

	 },
   plugins: [
    //   new webpack.ProvidePlugin({
    //     "$": "jquery",
    //     "jQuery": "jquery",
    //     "window.jQuery": "jquery",
    //     //"Vue": "vue"
    //   }),
     //  new OpenBrowserPlugin({
     //  	 url:'http://localhost:3000/index.html#!/home/cn'
     //  }),
      new webpack.optimize.UglifyJsPlugin({//used in production
        compress: {
          warnings: false // https://github.com/webpack/webpack/issues/1496
        }
      })

    ]

 };
