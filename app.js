var express = require('express');
var path = require('path');
var app = express();

app.get('/', function (req, res) {
  res.render('index.html');
});

app.use('/', express.static(path.join(__dirname, '')));

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
